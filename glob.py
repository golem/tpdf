import configparser

# Read configuration files (latest files in list override previous settings)
conf = configparser.ConfigParser()
conf.read(['conf/conf.ini', 'conf/conf.custom.ini'])

version = '1811.alpha'
