class TPDF {

    static display() {
        // console.log(TPDF.xhr.responseText);

        if (TPDF.xhr.readyState == 4 && TPDF.xhr.status == 200) {
            /* Clean up output div */
            while (TPDF.output.firstChild) {
                TPDF.output.removeChild(TPDF.output.firstChild);
            }

            /* Take up json */
            var results = JSON.parse(TPDF.xhr.responseText);

            var lastUpdateDate = new Date(results['lastupdate'] * 1000);
            var pLastUpdate = document.createElement('p');
            pLastUpdate.appendChild(document.createTextNode('Last database update: ' + lastUpdateDate.getFullYear() + '-' + (lastUpdateDate.getMonth() + 1) + '-' + lastUpdateDate.getDate()));
            TPDF.output.appendChild(pLastUpdate);

            for (var i in results['books']) {
                var dtAuthors = document.createElement('dt');
                dtAuthors.appendChild(document.createTextNode('Authors'));
                var dtPublisher = document.createElement('dt');
                dtPublisher.appendChild(document.createTextNode('Publisher'));
                var dtYear = document.createElement('dt');
                dtYear.appendChild(document.createTextNode('Year'));
                var dtPages = document.createElement('dt');
                dtPages.appendChild(document.createTextNode('Pages'));
                var dtISBN = document.createElement('dt');
                dtISBN.appendChild(document.createTextNode('ISBN'));
                var dtId = document.createElement('dt');
                dtId.appendChild(document.createTextNode('ID'));

                var ddAuthors = document.createElement('dd');
                var authorsString = '';
                for (var j in results['books'][i]['authors']) {
                    authorsString += results['books'][i]['authors'][j];
                    if (j < results['books'][i]['authors'].length - 1) authorsString += ', ';
                }
                ddAuthors.appendChild(document.createTextNode(authorsString));

                var ddPublisher = document.createElement('dd');
                ddPublisher.appendChild(document.createTextNode(results['books'][i]['publisher']));
                var ddYear = document.createElement('dd');
                if (results['books'][i]['year'] != undefined) ddYear.appendChild(document.createTextNode(results['books'][i]['year']));
                var ddPages = document.createElement('dd');
                if (results['books'][i]['page'] != undefined) ddPages.appendChild(document.createTextNode(results['books'][i]['page']));
                var ddISBN = document.createElement('dd');
                if (results['books'][i]['isbn'] != undefined) ddISBN.appendChild(document.createTextNode(results['books'][i]['isbn']));
                var ddId = document.createElement('dd');
                ddId.appendChild(document.createTextNode(results['books'][i]['id']));


                var bookElement = document.createElement('section');
                var titleElement = document.createElement('h2');
                var coverElement = document.createElement('img');

                var dlElement = document.createElement('dl');
                var authorsElement = document.createElement('ul');

                dlElement.appendChild(dtAuthors);
                dlElement.appendChild(ddAuthors);
                dlElement.appendChild(dtPublisher);
                dlElement.appendChild(ddPublisher);
                dlElement.appendChild(dtYear);
                dlElement.appendChild(ddYear);
                dlElement.appendChild(dtPages);
                dlElement.appendChild(ddPages);
                dlElement.appendChild(dtISBN);
                dlElement.appendChild(ddISBN);
                dlElement.appendChild(dtId);
                dlElement.appendChild(ddId);



                titleElement.appendChild(document.createTextNode(results['books'][i]['title']));
                if (results['books'][i]['cover'] != undefined) {
                    coverElement.src = 'output/images/' + results['books'][i]['cover'];
                    coverElement.alt = 'Cover of «' + results['books'][i]['title'] + '»';
                }
                else {
                    coverElement.src = 'frontend/tpdf-logo.png';
                    coverElement.alt = '[no image]';
                }


                bookElement.appendChild(titleElement);
                bookElement.appendChild(coverElement);
                bookElement.appendChild(dlElement);


                TPDF.output.appendChild(bookElement);
            }
        }
        else {
            TPDF.output.childNodes[0].textContent = 'API Error. Try later.';
        }

    }

    static submit() {
        /* Initialization */
        TPDF.xhr = new XMLHttpRequest();
        TPDF.output = document.getElementById('tpdfOutput');

        /* AJAX */
        TPDF.xhr.onload = TPDF.display;
        TPDF.xhr.open('GET',
            'main.py?format=json' +
            '&author=' + encodeURIComponent(document.querySelector('#tpdfForm input[name="author"]').value) +
            '&title=' + encodeURIComponent(document.querySelector('#tpdfForm input[name="title"]').value));
        TPDF.xhr.send(null);

        /* Nice GUI */
        while (TPDF.output.firstChild) {
            TPDF.output.removeChild(TPDF.output.firstChild);
        }
        var pStatus = document.createElement('p');
        pStatus.textContent = 'Searching...';
        TPDF.output.appendChild(pStatus);
    }

}

TPDF.xhr = null;
TPDF.output = null;
