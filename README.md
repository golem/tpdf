# TPDF - Tellico Parser anD Finder
A simple Tellico XML archive parser and finder for your book library, written in Python3.

TPDF runs as a web application via CGI (Common Gateway Interface).

## Install
### Apache
1. Modules needed (install and enable)
```
mod_env
cgi
cgid
```
2. Environment variables (add to virtualhost or htaccess)
```
SetEnv LANG en_US.UTF-8
```
3. Clone this repository in a directory inside your webserver document root
4. Pay attention to possible data leaks throught *output* directory

### other webservers
Should work, but not tested.

## Bugs
File bug report to GOLEM Linux Users Group [golem.linux.it](http://)

